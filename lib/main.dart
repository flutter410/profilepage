import 'package:flutter/material.dart';
enum APP_THEME{lIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),

      ),
      iconTheme: IconThemeData(
          color: Colors.indigo.shade500
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
          color: Colors.green.shade500
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();

}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme=APP_THEME.DARK;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: MyAppTheme.appThemeDark(),

      home: Scaffold(
        appBar:buildAppBarWidget(),
        body:buildBodyWinget(),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.threesixty),
                onPressed: (){
              setState(() {
                currentTheme == APP_THEME.DARK
      ? currentTheme =APP_THEME.lIGHT
      : currentTheme = APP_THEME.DARK;
              });
      },
),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.chat,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile ชิ้นส่วน
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
     title: Text("080-008-4261"),
     subtitle: Text("mobile"),
     trailing: IconButton(
       icon: Icon(Icons.message),
       color: Colors.indigo.shade500,
       onPressed: (){},
     ), // Action ข้างหลัง
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("480-445-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: (){},
    ), // Action ข้างหลัง
  );
}
Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160200@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St,Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: (){},
    ), // Action ข้างหลัง
  );
}
AppBar buildAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.white,
    leading: Icon(Icons.arrow_back,color: Colors.black,
    ),
    actions: <Widget>[
      IconButton( icon: Icon(Icons.star_border),color: Colors.black,
        onPressed: (){
          print("Contact is starred");
        },
      )
    ],
  );

}

Widget buildBodyWinget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://scontent.fbkk9-2.fna.fbcdn.net/v/t1.6435-9/119140134_1540729832801926_251694819729520252_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=174925&_nc_ohc=cIpglSBYHuYAX_d4pR_&_nc_ht=scontent.fbkk9-2.fna&oh=00_AfBBlxoA0XOghTYZYUV1CHYFgX3eBMYYwnPX28yP4xTgJw&oe=63C9F77B",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child:  Text(
                      "Thidarat Kodpat",
                      style: TextStyle(fontSize: 30),
                    )
                )
              ],
            ),
          ),
          Divider( // เส้น
            color: Colors.black,
          ),
          Container (
            margin: const EdgeInsets.only(top:8,bottom: 8),
            child:Theme(
              data: ThemeData(
               iconTheme: IconThemeData(
                 color: Colors.pink
               ) ,
              ),
            child: profileActionItems(),
            )

          ),
          Divider( // เส้น
            color: Colors.black,
          ),
          mobilePhoneListTile(), // เรียกใช้
          otherPhoneListTile() ,// เรียกใช้
          Divider( // เส้น
            color: Colors.black,
          ),
          emailListTile(),
          addressListTile(),

        ],
      ),
    ],
  );
}
Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),
    ],
  );
}


